Ext.define('sitemapModel', {
	extend: 'Ext.data.Model',
	fields: ['date_created', 'id', 'state', 'approved', 'site', 'date_finished', 'context_pages']
});

Ext.define('sitesModel', {
	extend: 'Ext.data.Model',
	fields: ['id', 'url']
});

var sitemapStore = Ext.create('Ext.data.Store', {
	model: 'sitemapModel',
	storeId: 'Sitemap',
	proxy: {
		type: 'ajax',
		url: '/ajax/sitemap/read',
		reader: {
			type: 'json',
			root: 'data',
			totalProperty: 'total'
		},
		api: {
			read: '/ajax/sitemap/read',
			create: '/ajax/sitemap/create',
			update: '/ajax/sitemap/update',
			destroy: '/ajax/sitemap/destroy'
		}
	},
	pageSize: 25,
	autoLoad: true,
	remoteFilter: true,
	remoteSort: true,
	autoSync: true
});

var sitesStore = Ext.create('Ext.data.Store', {
	model: 'sitesModel',
	storeId: 'Sites',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: '/ajax/sitemap/read',
		reader: {
			type: 'json',
			root: 'data',
			totalProperty: 'total'
		},
		api: {
			read: '/ajax/sites/read'
		}
	},
	autoLoad: true,
	remoteFilter: true,
	remoteSort: true,
	autoSync: true
});

var sitemapDocument = null;

var tab_sitemap = {
	title: 'Карта сайта',
	xtype: 'tabpanel',
	items: [{
			title: 'Карта сайта',
			layout: 'vbox',
			id: 'id_tab_sitemap',
			xtype: 'tabpanel',
			activeTab: 0,
			tabBar: {
				style: 'display: none;'
			},
			dockedItems: [{
					xtype: 'toolbar',
					dock: 'top',
					items: [{
							xtype: 'button',
							icon: '/assets/images/icons/arrow_refresh.png',
							text: 'Список',
							listeners: {
								click: function(button) {
									var panel = button.findParentByType('tabpanel');
									var grid = Ext.getCmp('id_tab_sitemap_grid');
									panel.setActiveTab('id_tab_sitemap_grid');
									grid.getStore().loadPage(1);
								}
							}
						}, {
							xtype: 'button',
							icon: '/assets/images/icons/add.png',
							text: 'Создать',
							listeners: {
								click: function(button) {
									sitemapDocument = Ext.create('Sitemap.form');
									sitemapDocument.placeFormData();
									var panel = button.findParentByType('tabpanel');
									panel.setActiveTab('id_tab_sitemap_form');
								}
							}
						}/*, {
						 xtype: 'tbfill'
						 }, {
						 xtype: 'button',
						 text: 'Right Button'
						 }*/]
				}],
			items: [{
					title: 'Список',
					id: 'id_tab_sitemap_grid',
					xtype: 'grid',
					store: sitemapStore,
					columns: [
						{
							text: 'Дата',
							flex: 2,
							dataIndex: 'date_created',
							sortable: false
						},
						{
							text: 'Номер',
							flex: 1,
							dataIndex: 'id',
							sortable: false
						},
						{
							text: 'Статус',
							flex: 1,
							dataIndex: 'state',
							sortable: false,
							renderer: function(value, metaData, record, rowIndex, colIndex, store){
								return sitemap_states[value];
							}
						},
						{
							text: 'Проведён',
							flex: 1,
							dataIndex: 'approved',
							sortable: false,
							renderer: function(value, metaData, record, rowIndex, colIndex, store){
								return ((value == '0') ? 'нет' : 'да');
							}
						},
						{
							text: 'Сайт',
							flex: 3,
							dataIndex: 'site',
							sortable: false
						},
						{
							text: 'Дата построения',
							flex: 1,
							dataIndex: 'date_finished',
							sortable: false,
							renderer: function(value, metaData, record, rowIndex, colIndex, store){
								return ((value === null) ? 'ещё не завершена' : value);
							}
						},
						{
							text: 'Контекст ко-во страниц',
							flex: 1,
							dataIndex: 'context_pages',
							sortable: false
						},
						{
							xtype: 'actioncolumn',
							width: 50,
							items: [
								{
									icon: '/assets/images/icons/delete.png',
									tooltip: 'Удалить документ',
									handler: function(grid, rowIndex, colIndex) {
										var rec = grid.getStore().getAt(rowIndex);
										Ext.MessageBox.confirm('Подтвердите удаление', 'Вы собираетесь удалить документ номер ' + rec.get('id'), function(btn) {
											if (btn == 'yes')
											{
												grid.getStore().removeAt(rowIndex);
											}
										});
									}
								}
							]
						}
					],
					viewConfig: {
						stripeRows: true
					},
					listeners : {
						itemdblclick: function(dv, record, item, index, e) {
							sitemapDocument = Ext.create('Sitemap.form',record.internalId);
							sitemapDocument.placeFormData();
							var panel = Ext.getCmp('id_tab_sitemap');
							panel.setActiveTab('id_tab_sitemap_form');	
						}
					},
					bbar: Ext.create('Ext.PagingToolbar', {
						store: sitemapStore
					})
				}, {
					title: 'Форма',
					id: 'id_tab_sitemap_form',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					padding: '10 10',
					border: 0
				}]
		}, {
			title: 'Пересечение',
			html: "My content was added during construction.",
			disabled: true
		},
		{
			title: 'Рекомендации',
			html: "My content was added during construction.",
			disabled: true
		},
		{
			title: 'Контекст',
			html: "My content was added during construction.",
			disabled: true
		}
	]
}