var nav_menu = {
	expanded: true,
	children: [{
			leaf: true,
			text: 'Карта сайта',
			iconCls: 'x-icon-card',
			tabId: 'id_tab_sitemaps'
		}]
};

var tabs = [tab_sitemap];

Ext.application({
	name: 'Fructus',
	launch: function() {
		Ext.create('Ext.container.Viewport', {
			layout: 'border',
			items: [
				{
					region: 'north',
					xtype: 'container',
					style: 'font-family: arial; font-size: 14pt; color: #15428B;',
					height: 40,
					html: '<div class="b-header"><div class="b-header-right">Вы вошли как <b>guest</b> (<a class="b-header-right-link" href="/logout">Выйти</a>)</div>Fructus SAAS :: Карта сайта</div>'
				}, {
					region: 'west',
					collapsible: true,
					title: 'Навигация',
					width: 255,
					margins: '0 0 5 5',
					split: true,
					layout: 'fit',
					stateful: true,
					stateId: 'menuPanelState',
					stateEvents: ['collapse', 'expand'],
					items: [{
							xtype: 'treepanel',
							border: false,
							id: 'id_treepanel_menu',
							autoScroll: true,
							rootVisible: false,
							root: nav_menu,
							listeners: {
								select: function(treepanel, record, index) {
									var tabpanel = Ext.getCmp('id_tabpanel_main');

									if (record.raw.tabId) {
										tabpanel.setActiveTab(record.raw.tabId);
									}
								}
							}
						}]
				}, {
					region: 'center',
					layout: 'fit',
					margins: '0 5 5 0',
					frame: true,
					layout: 'fit',
							items: [{
							id: 'id_tabpanel_main',
							xtype: 'tabpanel',
							activeTab: 0,
							border: 0,
							tabBar: {
								style: 'display: none;'
							},
							activeTab: 0,
							items: tabs
						}
					]
				}
			]
		});
	}
});

Ext.onReady(function() {
	var tree = Ext.getCmp('id_treepanel_menu');
	tree.getSelectionModel().select(tree.getRootNode().findChild('leaf', true, true));
});