Ext.Ajax.timeout = 120000;
Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Sitemap', 'assets/js/classes/Sitemap');

var sitemap_states = new Array;
sitemap_states[0] = 'не обработана';
sitemap_states[1] = 'в процессе';
sitemap_states[2] = 'завершена';