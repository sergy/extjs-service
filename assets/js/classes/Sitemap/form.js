Ext.define('stopWordsModel', {
	extend: 'Ext.data.Model',
	fields: ['id', 'word']
});

Ext.define('pagesModel', {
	extend: 'Ext.data.Model',
	fields: ['id', 'url', 'status', 'deep', 'title', 'description', 'keywords', 'redirect', 'weight']
});

Ext.define('linksModel', {
	extend: 'Ext.data.Model',
	fields: ['url', 'text', 'title']
});

Ext.define('Sitemap.form', {
	config: {
		id: null,
		date_created: null,
		date_finished: null,
		state: null,
		site_id: null,
		noindex: null
	},
	statics: {
		states: sitemap_states
	},
	form: null,
	stopWordsStore: Ext.create('Ext.data.Store', {
		model: 'stopWordsModel',
		storeId: 'StopWords',
		proxy: {
			type: 'ajax',
			url: '/ajax/sitemap/readWords',
			reader: {
				type: 'json',
				root: 'data',
				totalProperty: 'total'
			},
			api: {
				read: '/ajax/sitemap/readWords',
				destroy: '/ajax/sitemap/deleteWords'
			}
		},
		pageSize: 25,
		autoLoad: true,
		remoteFilter: true,
		remoteSort: true,
		autoSync: true
	}),
	pagesStore: Ext.create('Ext.data.Store', {
		model: 'pagesModel',		
		proxy: {
			type: 'ajax',
			url: '/ajax/sitemap/readPages',
			extraParams: {
				documentId: null
			},
			reader: {
				type: 'json',
				root: 'data',
				totalProperty: 'total'
			}
		},
		pageSize: 25,
		autoLoad: true,
		remoteFilter: true,
		remoteSort: true,
		autoSync: true
	}),
	linksStore: Ext.create('Ext.data.Store', {
		model: 'linksModel',		
		proxy: {
			type: 'ajax',
			url: '/ajax/sitemap/readLinks',
			extraParams: {
				documentId: null,
				pageId: null,
				direction: null
			},
			reader: {
				type: 'json',
				root: 'data',
				totalProperty: 'total'
			}
		},
		pageSize: 25,
		autoLoad: false,
		remoteFilter: true,
		remoteSort: true,
		autoSync: true
	}),
	getUploadWin: function() {
		var me = this;
		var uploadForm = new Ext.FormPanel({
			xtype: "form",
			fileUpload: true,
			frame: true,
			isUpload: true,
			width: 310,
			autoHeight: true,
			items: [{
				xtype: 'fileuploadfield',
				fieldLabel: 'Файл',
				labelWidth: 35,
				width: 280,
				buttonText: 'Выбрать файл',
				name: 'file',
				padding: '10'
			}],
			buttons: [{
				text: 'Ok',
				handler: function() {
					if (uploadForm.getForm().isValid()) {
						uploadForm.getForm().submit({
							url: '/ajax/sitemap/importWords',
							waitMsg: 'Обработка файла ...',
							params: {
								documentId: me.id
							},
							success: function(form, action) {
								json = Ext.decode(action.response.responseText);
								Ext.Msg.alert('Импорт завершён', json.message);
								me.stopWordsStore.loadPage(1);
								uploadWin.close();
							},
							failure: function(form, action) {
								json = Ext.decode(action.response.responseText);
								switch (action.failureType) {
									case Ext.form.action.Action.CLIENT_INVALID:
										Ext.Msg.alert('Ошибка', 'Проверьте правильность введённых данных');
										break;
									case Ext.form.action.Action.CONNECT_FAILURE:
										Ext.Msg.alert('Ошибка', 'Ошибка соединения');
										break;
									case Ext.form.action.Action.SERVER_INVALID:
										if (json.message)
											Ext.Msg.alert('Ошибка', json.message);
										else
											Ext.Msg.alert('Ошибка', 'Сервер вернул некорректный ответ');
								}
							}
						});
					}
				}
			}, {
				text: 'Отмена',
				handler: function() {
					uploadWin.close();
				}
			}]
		});

		var uploadWin = new Ext.Window({
			width: 310,
			minWidth: 310,
			minHeight: 80,
			layout: 'fit',
			border: false,
			closable: true,
			collapsible: false,
			modal: true,
			title: 'Выберите файл',
			items: uploadForm
		});
		
		uploadWin.show();
		
	},
	placeFormData: function() {
		var me = this;
		me.date_finished = (me.date_finished === null) ? 'ещё не завершена' : me.date_finished;
		me.form = Ext.create('Ext.container.Container', {
			border: 0,
			items: [{
					xtype: 'container',
					flex: 3,
					height: 150,
					maxHeight: 150,
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'container',
							border: 0,
							flex: 1,
							items: [{
									xtype: 'displayfield',
									fieldLabel: 'Номер',
									id: 'id_tab_sitemap_form_number',
									value: me.id,
									submitValue: true
								}, {
									xtype: 'combobox',
									fieldLabel: 'Сайт',
									id: 'id_tab_sitemap_form_site_id',
									plugins: ['clearbutton'],
									displayField: 'url',
									valueField: 'id',
									value: me.site_id,
									store: sitesStore,
									listeners: {
										select: function(field, value) {
											Ext.getCmp('id_tab_sitemap_applyButton').setDisabled(false);
										}
									}
								}, {
									xtype: 'button',
									text: 'Выполнить',
									listeners: {
										click: function(button) {
											Ext.Ajax.request({
												url: '/ajax/sitemap/createSitemap',
												method: 'POST',
												headers: {'Content-Type': 'application/json'},
												jsonData: {
													documentId: sitemapDocument.id,
													site_id: Ext.getCmp('id_tab_sitemap_form_site_id').getValue(),
													noindex: Ext.getCmp('id_tab_sitemap_form_noindex').getValue()
												},
												waitTitle: 'Ожидание',
												waitMsg: 'Сохранение задания...',
												scope: this,
												callback: function(options, success, response) {
													if (success)
													{
														var json = Ext.decode(response.responseText);
														if (json.success !== true)
														{
															Ext.MessageBox.show({
																title: 'Ошибка',
																msg: json.message,
																buttons: Ext.MessageBox.OK
															});
														}
														else
														{
															Ext.MessageBox.show({
																title: 'Успешно',
																msg: json.message,
																buttons: Ext.MessageBox.OK
															});
														}
													}
													else
													{
														Ext.MessageBox.show({
															title: 'Ошибка',
															msg: 'Не удалось сохранить изменения в документе. Сервер вернул некорректный ответ.',
															buttons: Ext.MessageBox.OK
														});
													}
												}
											});
										}
									}
								}]
						}, {
							xtype: 'container',
							border: 0,
							flex: 1,
							items: [{
									xtype: 'displayfield',
									fieldLabel: 'Дата',
									id: 'id_tab_sitemap_form_created',
									value: me.date_created
								}, {
									xtype: 'numberfield',
									id: 'id_tab_sitemap_form_noindex',
									fieldLabel: 'Глубина noindex',
									value: me.noindex,
									minValue: 0,
									listeners: {
										change: function(field, value) {
											if (value > 0)
												Ext.getCmp('id_tab_sitemap_applyButton').setDisabled(false);
										}
									}
								}]
						}, {
							xtype: 'container',
							border: 0,
							flex: 2,
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							items: [
								{
									xtype: 'grid',
									flex: 12,
									store: me.stopWordsStore,
									collapsible: false,
									height: 150,
									maxHeight: 150,
									margins: '0 5',
									dockedItems: [{
											xtype: 'toolbar',
											dock: 'top',
											items: [{
													text: 'Действия со стоп-словами:',
													icon: '/assets/images/icons/application_view_list.png',
													menu: {
														xtype: 'menu',
														plain: true,
														items: [{
																text: 'Очистить список',
																icon: '/assets/images/icons/application_delete_1.png',
																handler: function() {
																	Ext.MessageBox.confirm('Подтвердите удаление', 'Вы собираетесь удалить все стоп-слова из этого докумунта?', function(btn) {
																		if (btn == 'yes')
																			sitemapDocument.stopWordsStore.removeAll();
																	});
																}
															}, {
																text: 'Импорт списка',
																icon: '/assets/images/icons/cart_put_1.png',
																handler: function() {
																	me.getUploadWin();
																}
															}, {
																text: 'Экспорт списка',
																icon: '/assets/images/icons/cart_remove_1.png',
																handler: function() {
																	Ext.Ajax.request({
																		url: '/ajax/sitemap/exportWords',
																		jsonData: {
																			documentId: me.id
																		},
																		method: 'POST',
																		headers: {'Content-Type': 'application/json'},
																		waitTitle: 'Ожидание',
																		waitMsg: 'Формирование файла...',
																		scope: this,
																		callback: function(options, success, response) {
																			if (success)
																			{
																				var json = Ext.decode(response.responseText);
																				if (json.success === true)
																				{
																					Ext.DomHelper.append(document.body, {
																						tag: 'iframe',
																						id: 'downloadIframe',
																						frameBorder: 0,
																						width: 0,
																						height: 0,
																						css: 'display:none;visibility:hidden;height:0px;',
																						src: json.path
																					});
																				} else {
																					Ext.MessageBox.show({
																						title: 'Ошибка',
																						msg: json.message,
																						buttons: Ext.MessageBox.OK
																					});
																				}
																			}
																			else
																			{
																				Ext.MessageBox.show({
																					title: 'Ошибка',
																					msg: 'Не удалось сформировать файл. Сервер вернул некорректный ответ.',
																					buttons: Ext.MessageBox.OK
																				});
																			}
																		}
																	});
																}
															}]
													}
												}, {
													text: 'Действия с пропущенными:',
													icon: '/assets/images/icons/application_view_list.png',
													menu: {
														xtype: 'menu',
														plain: true,
														items: [{
																text: 'Просмотр пропущенных',
																icon: '/assets/images/icons/application_form_magnify_1.png'
															}, {
																text: 'Очистить пропущенные',
																icon: '/assets/images/icons/application_delete_1.png'
															}]
													}
												}]
										}],
									columns: [
										{
											text: '№',
											dataIndex: 'id',
											flex: 1,
											sortable: false
										},
										{
											text: 'Слово',
											dataIndex: 'word',
											flex: 20,
											sortable: false
										}
									],
									viewConfig: {
										stripeRows: true
									},
									bbar: Ext.create('Ext.PagingToolbar', {
										store: me.stopWordsStore
									})
								}
							]
						}
					]
				}, {
					xtype: 'container',
					padding: '10 0',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'grid',
							flex: 2,
							id: 'id_tab_sitemap_result_pages',
							store: me.pagesStore,
							collapsible: false,
							columns: [
								{
									text: '№',
									dataIndex: 'id',
									flex: 1,
									sortable: false
								},
								{
									text: 'Url',
									dataIndex: 'url',
									flex: 20,
									sortable: false
								},
								{
									text: 'Статус',
									dataIndex: 'status',
									flex: 5,
									sortable: false
								},
								{
									text: 'Глубина',
									dataIndex: 'deep',
									flex: 5,
									sortable: false
								},
								{
									text: 'Title',
									dataIndex: 'title',
									flex: 5,
									sortable: false
								},
								{
									text: 'Description',
									dataIndex: 'description',
									flex: 5,
									sortable: false
								},
								{
									text: 'Keywords',
									dataIndex: 'keywords',
									flex: 5,
									sortable: false
								},
								{
									text: 'Редирект',
									dataIndex: 'redirect',
									flex: 5,
									sortable: false
								},
								{
									text: 'Вес',
									dataIndex: 'weight',
									flex: 5,
									sortable: false
								}
							],
							viewConfig: {
								stripeRows: true
							},
							listeners : {
								itemdblclick: function(dv, record, item, index, e) {
									me.linksStore.getProxy().extraParams.documentId = me.id;
									me.linksStore.getProxy().extraParams.pageId = record.internalId;
									me.linksStore.getProxy().extraParams.direction = Ext.getCmp('id_tab_sitemap_result_links_direction').getValue();
									me.linksStore.loadPage(1);
								}
							},
							bbar: Ext.create('Ext.PagingToolbar', {
								store: me.pagesStore,
								displayInfo: true,
								displayMsg : 'Показано {0} - {1} из {2}'
							})
						}, {
							xtype: 'grid',
							id: 'id_tab_sitemap_result_links',
							flex: 1,
							store: me.linksStore,
							collapsible: false,
							margins: '0 5',
							dockedItems: [{
								xtype: 'toolbar',
								dock: 'top',
								layout: 'hbox',
								items: [{
									xtype: 'radiogroup',
									id: 'id_tab_sitemap_result_links_direction',
									fieldLabel: 'Ссылки',
									labelWidth: 35,
									layout: 'hbox',
									items: [
										{boxLabel: 'с', name: 'links_direction', inputValue:'from', checked: true, padding: '0 15px 0'},
										{boxLabel: 'на', name: 'links_direction', inputValue: 'to'}
									],
									listeners: {
										change: function() {
											me.linksStore.getProxy().extraParams.documentId = me.id;
											me.linksStore.getProxy().extraParams.direction = this.getValue();
											me.linksStore.loadPage(1);
										}
									}
								}]
							}],
							columns: [
								{
									text: 'Url',
									dataIndex: 'url',
									flex: 20,
									sortable: false
								},
								{
									text: 'Текст ссылки',
									dataIndex: 'text',
									flex: 5,
									sortable: false
								},
								{
									text: 'Title ссылки',
									dataIndex: 'title',
									flex: 5,
									sortable: false
								}
							],
							viewConfig: {
								stripeRows: true
							},
							bbar: Ext.create('Ext.PagingToolbar', {
								store: me.linksStore,
								displayInfo: true,
								displayMsg : 'Показано {0} - {1} из {2}'
								
							})
						}
					]
				}, {
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'container',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							flex: 2,
							items: [/*
								{
									xtype: 'button',
									margins: '0 5 0 0',
									text: 'Найти (Alt + E)'
								}, {
									xtype: 'button',
									margins: '0 5 0 0',
									text: 'Очистить failied'
								}, {
									xtype: 'button',
									margins: '0 5 0 0',
									text: 'К следующему из плана (Alt + 1)'
								}, */{
									xtype: 'button',
									id: 'id_tab_sitemap_applyButton',
									margins: '0 10 0 0',
									disabled: true,
									text: 'Записать (Alt + s)',
									handler: function() {
										Ext.Ajax.request({
											url: '/ajax/sitemap/save',
											method: 'POST',
											headers: {'Content-Type': 'application/json'},
											jsonData: {
												documentId: sitemapDocument.id,
												site_id: Ext.getCmp('id_tab_sitemap_form_site_id').getValue(),
												noindex: Ext.getCmp('id_tab_sitemap_form_noindex').getValue()
											},
											waitTitle: 'Ожидание',
											waitMsg: 'Сохранение изменений в документе...',
											scope: this,
											callback: function(options, success, response) {
												if (success)
												{
													var json = Ext.decode(response.responseText);
													if (json.success !== true)
													{
														Ext.MessageBox.show({
															title: 'Ошибка',
															msg: json.message,
															buttons: Ext.MessageBox.OK
														});
													}
													else
													{
														Ext.getCmp('id_tab_sitemap_applyButton').setDisabled(true);
													}
												}
												else
												{
													Ext.MessageBox.show({
														title: 'Ошибка',
														msg: 'Не удалось сохранить изменения в документе. Сервер вернул некорректный ответ.',
														buttons: Ext.MessageBox.OK
													});
												}
											}
										});
									}
								}, {
									xtype: 'displayfield',
									id: 'id_tab_sitemap_date_finished',
									labelWidth: 150,
									fieldLabel: 'Дата построения карты',
									value: me.date_finished
								}
							]
						}

					]
				}]
		});

		var form = Ext.getCmp('id_tab_sitemap_form');
		form.removeAll(true);
		form.add(me.form);
		form.doLayout();

		me.stopWordsStore.getProxy().extraParams.documentId = me.id;
		me.stopWordsStore.loadPage(1);
		
		me.pagesStore.getProxy().extraParams.documentId = me.id;
		me.pagesStore.loadPage(1);
		
		sitesStore.load();
	},
	constructor: function(id) {
		if (id)
			var url = '/ajax/sitemap/document/' + id;
		else
			var url = '/ajax/sitemap/new';

		Ext.Ajax.request({
			url: url,
			async: false,
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			waitTitle: 'Ожидание',
			waitMsg: 'Получения информации из документа...',
			scope: this,
			callback: function(options, success, response) {
				if (success)
				{
					var json = Ext.decode(response.responseText);
					if (json.success === true)
					{
						this.initConfig(json.data);
					} else {
						Ext.MessageBox.show({
							title: 'Ошибка',
							msg: json.message,
							buttons: Ext.MessageBox.OK
						});
						return false;
					}
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Ошибка',
						msg: 'Не удалось создать документ. Сервер вернул некорректный ответ.',
						buttons: Ext.MessageBox.OK
					});
					return false;
				}
			}
		});
	}
});