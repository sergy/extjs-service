<?php
require_once $docpath . '/cron/nokogiri.php';

$deep = 0;
$query = http_build_query(json_decode($task['data'], true));
$data = json_decode(getTaskData($domain.'/cron/' . $task['task'] . '/?' . $query), true);

if ($data && isset($data['data']['site']['url'])) {
	echo 'start '.date('H:i:s');
	$scheme = 'http://';
	$host = $data['data']['site']['url'];
	if($data['data']['site']['seo_wo_www'] == 0) $host = 'www.'.$host;
	$dirName = '/db/' . date('Y-m') . '/';
	$dir = $docpath . $dirName;
	if (!is_dir($dir))
		mkdir($dir, 0777);
	$dbname = 'sitemapTask_' . $host . '_' . $task['id'];
	$dbpath = $dir . $dbname;
	if (is_file($dbpath))
		unlink($dbpath);

	$db_conn = 'sqlite:' . $dbpath;
	try {
		$dbh = new PDO($db_conn);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$dbh->query("CREATE TABLE 'url' ('url' TEXT PRIMARY KEY NOT NULL, 'status' TEXT, 'deep' INTEGER, 'title' TEXT, 'description' TEXT, 'keywords' TEXT, 'redirect' TEXT, 'weight' TEXT)");
		$dbh->query("CREATE TABLE 'url_external' ('url' TEXT PRIMARY KEY NOT NULL)");
		$dbh->query("CREATE TABLE 'url_links' ('from' INTEGER NOT NULL, 'to' INTEGER NOT NULL, 'title' text NULL, 'text' text NULL, 'external' INTEGER NULL)");
	} catch (Exception $e) {
		var_dump($e);
		exit;
	}
	
	geturls($scheme . $host, $host, $scheme, $dbh, $deep);
	
	$query = http_build_query(array('documentId' => $task['document_id'], 'taskId' => $task['id'], 'dbpath' => $dirName . $dbname));

	getTaskData($domain.'/cron/sitemap/finish/?' . $query);
	echo 'stop '.date('H:i:s');
}

function getPageIdByUrl($page,$deep,&$dbh)
{
	$q = $dbh->prepare('SELECT rowid FROM `url` WHERE `url` = :url LIMIT 1');
	$q->bindValue(':url',$page);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_ASSOC);
	if(isset($r['rowid'])) $urlId = $r['rowid'];
	else {
		$q = $dbh->prepare('INSERT INTO `url` (`url`,`deep`) VALUES (:url, :deep)');
		$q->bindValue(':url',$page);
		$q->bindValue(':deep',$deep);
		$q->execute();
		
		$q = $dbh->query('SELECT last_insert_rowid() as rowid FROM `url` LIMIT 1');
		$r = $q->fetch(PDO::FETCH_ASSOC);
		if(isset($r['rowid'])) $urlId = $r['rowid'];
	}
	
	return $urlId;
}

function getExternalPageIdByUrl($page,&$dbh)
{
	$q = $dbh->prepare('SELECT rowid FROM `url_external` WHERE `url` = :url LIMIT 1');
	$q->bindValue(':url',$page);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_ASSOC);
	if(isset($r['rowid'])) $urlId = $r['rowid'];
	else {
		$q = $dbh->prepare('INSERT INTO `url_external` (`url`) VALUES (:url)');
		$q->bindValue(':url',$page);
		$q->execute();
		
		$q = $dbh->query('SELECT last_insert_rowid() as rowid FROM `url_external` LIMIT 1');
		$r = $q->fetch(PDO::FETCH_ASSOC);
		if(isset($r['rowid'])) $urlId = $r['rowid'];
	}
	
	return $urlId;
}

function setUrlToUrl($from,$to,$title,$text,$external,&$dbh)
{
	$q = $dbh->prepare('INSERT INTO `url_links` (`from`,`to`,`title`,`text`,`external`) VALUES (:from, :to, :title, :text, :external)');
	$q->bindValue(':from',$from);
	$q->bindValue(':to',$to);
	$q->bindValue(':title',$title);
	$q->bindValue(':text',$text);
	$q->bindValue(':external',$external);
	$q->execute();
}

function geturls($page, $host, $scheme, &$dbh, $deep = 0,&$urls = array()) {
	
	if(in_array($page, $urls)) return;
	
	$urls[] = $page;
	echo $page."\r\n";
	$urlId = getPageIdByUrl($page,$deep,$dbh);
	if($deep == 0) $deep++;
	if(isset($urlId) && !empty($urlId))
	{
		$q = $dbh->prepare('UPDATE `url` SET `status` = :code, `redirect` = :redirect WHERE `rowid` = :id');
		
		list($code,$redirect,$charset,$html) = serverAnswer($page);
		$isfile = false;
		
		$q->bindValue(':code',$code);
		$q->bindValue(':redirect',$redirect);
		$q->bindValue(':id',$urlId);
		$q->execute();
		if($code == 200)
		{
			if($charset != 'UTF-8')
			{
				if(strstr($charset,'/'))
				{
					$q = $dbh->prepare('UPDATE `url` SET `status` = :code WHERE `rowid` = :id');
					$q->bindValue(':code',$code.' '.$charset);
					$q->bindValue(':id',$urlId);
					$q->execute();
					$isfile = true;
				}
				else
				{
					$html = iconv($charset,'UTF-8',$html);
				}
			}
			if(!$isfile)
			{
				$saw = nokogiri::fromHtmlNoCharset($html);
				$title = $saw->get('title')->toArray();
				$title = isset($title[0]['#text'][0]) ? $title[0]['#text'][0] : '';
				$description = '';
				$keywords = '';
				foreach($saw->get('meta') as $meta)
				{
					if(isset($meta['name']) && $meta['name'] == 'description')
					{
						$description = $meta['content'];
					}

					if(isset($meta['name']) && $meta['name'] == 'keywords')
					{
						$keywords = $meta['content'];
					}
				}

				$q = $dbh->prepare('UPDATE `url` SET `title` = :title, `description` = :description, `keywords` = :keywords WHERE `rowid` = :id');
				$q->bindValue(':title',$title);
				$q->bindValue(':description',$description);
				$q->bindValue(':keywords',$keywords);
				$q->bindValue(':id',$urlId);
				$q->execute();
				
				$nextDeep = array();
				
				foreach($saw->get('a') as $a)
				{					
					$title = isset($a['title']) ? $a['title'] : '';
					$href = isset($a['href']) ? $a['href'] : '';
					$text = isset($a['#text'][0]) ? $a['#text'][0] : '';

					//дописываем домен если его нет в ссылке
					if (!strstr($href, $scheme . $host)) {

						$urlinfo = @parse_url($href);
						if(!isset($urlinfo['host']))
						{
							if(substr($href,0,2) == '//')
							{
								$href = 'http:' . $href;
							}
							else
							{
								$href = $scheme . $host . str_replace('//','/','/' . $href);
							}
						}
					}

					$href = preg_replace("/#.*/X", "", $href);
					$urlinfo = @parse_url($href);
					if (!isset($urlinfo['path'])) {
						$urlinfo['path'] = NULL;
					}
					//не пропускаем ссылки на левые домены
								//и ссылки со стоп словами (плюс нужно учитывать noindex)					TODO!!!!
					if (isset($urlinfo['host']) && $urlinfo['host'] == $host AND !strstr($href, '@')) {
						//получаем id ссылки
						$subUrlId = getPageIdByUrl($href,$deep,$dbh);

						echo $urlId.'/'.$subUrlId;
						//записываем что на эту страницу ссылается текущая
						setUrlToUrl($urlId,$subUrlId,$title,$text,'0',$dbh);
						$nextDeep[] = $href;
					} elseif(isset($urlinfo['host']) && $urlinfo['host'] != $host && !empty($href))
					{
						$subUrlId = getExternalPageIdByUrl($href,$dbh);
						setUrlToUrl($urlId,$subUrlId,$title,$text,'1',$dbh);
					}
				}
				
				if(!empty($nextDeep))
				{
					$deep++;
					foreach($nextDeep as $href)
					{
						//переходим дальше
						geturls($href, $host, $scheme, $dbh, $deep, $urls);
					}
				}
				else return;
			}
			else return;
		}
		else
			return;
	}
	else
		return;
}

function serverAnswer($url) {
	$code = '';
	$redirect = '';
	$headers = array('Content-type: text/html; charset=UTF-8');
	$user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0";
	$ctmp = curl_init();
	curl_setopt($ctmp, CURLOPT_URL, $url);
	curl_setopt($ctmp, CURLOPT_HEADER, true);
	curl_setopt($ctmp, CURLOPT_USERAGENT, $user_agent);
	curl_setopt($ctmp, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ctmp, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ctmp, CURLOPT_VERBOSE, false);
	curl_setopt($ctmp, CURLOPT_TIMEOUT, 5);
	curl_setopt($ctmp, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ctmp, CURLOPT_SSLVERSION, 3);
	curl_setopt($ctmp, CURLOPT_SSL_VERIFYHOST, false);
	$page = curl_exec($ctmp);
	if($page === false)
	{
		print_r(curl_error($ctmp));
		curl_close($ctmp);
		return false;
	}
	else
	{
		$code = curl_getinfo($ctmp, CURLINFO_HTTP_CODE);
		$charset = curl_getinfo($ctmp, CURLINFO_CONTENT_TYPE);
		preg_match('/charset=(.+)/', $charset, $found);
		if(!isset($found[1])) {$p = '';}
		else {$p = trim($found[1]);}
		if(!empty($p) && $p != "")
		{
			$charset = $p;
			$encot = true;
		}

		if ($code == 301 || $code == 302)
		{
			preg_match_all('/^Location:(.*)$/mi', $page, $matches);
			$redirect = (isset($matches[1]) && !empty($matches[1])) ? trim($matches[1][0]) : 'no redirect';
		}

		curl_close($ctmp);
		return array($code,$redirect,$charset,$page);
	}
}

function getTaskData($url) {
	$headers = array('Content-type: text/html; charset=UTF-8');
	$user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0";
	$ctmp = curl_init();
	curl_setopt($ctmp, CURLOPT_URL, $url);
	curl_setopt($ctmp, CURLOPT_HEADER, false);
	curl_setopt($ctmp, CURLOPT_USERAGENT, $user_agent);
	curl_setopt($ctmp, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ctmp, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ctmp, CURLOPT_VERBOSE, false);
	curl_setopt($ctmp, CURLOPT_TIMEOUT, 30);
	curl_setopt($ctmp, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	curl_setopt($ctmp, CURLOPT_USERPWD, "fructus:saasfructus");
	$page = curl_exec($ctmp);
	if($page === false)
	{
		print_r(curl_error($ctmp));
		curl_close($ctmp);
		return false;
	}
	else
	{		
		return $page;
	}
}

function gzdecode($data) 
{ 
   return gzinflate(substr($data,10,-8)); 
} 