<?php
exit;
	$data = file_get_contents('sites.xml');
	$xml = new SimpleXMLElement($data);
	$sites = array();
	$stopAttr = array('FTP','Login','Password','УчитыватьВнешниеСсылки','ftp_id');
	$attributesMap = array(
		'ПометкаУдаления' => 'delete_mark',
		'Предопределенный' => 'predefined',
		'Код' => 'code',
		'Тематика' => 'theme_id',
		'КореньСайта' => 'site_root',
		'ПоследняяФраза' => 'last_phrase_id',
		'ДатаСоздания' => 'date_created',
		'ТипСайта' => 'type_id',
		'Приоритет' => 'priority',
		'АвтоматическаяПроверкаПозиций' => 'positions_autochecking',
		'КолонкиПоХронологии' => 'columns_by_chronology',
		'РазукрашиватьОтчет' => 'decorate_report',
		'продвигаетсяБезWWW' => 'seo_wo_www',
		'АдресКаталогаСтатей' => 'articles_address',
		'liex_id' => 'liex_id',
		'ВыводитьПервыеПозиции' => 'show_first_positions',
		'КоличествоПервыхПозиций' => 'count_first_positions',
		'ВыводитьПроценты' => 'show_percents',
		'АвтоматическаяПроверкаМета' => 'meta_autochecking',
		'sape_id' => 'sape_id',
		'ИспользуетГруппировкиЗапросов' => 'use_request_groups',
		'ПервыеПозицииСНачалаПериода' => 'first_positions_on_period_start',
		'выводитьВТоп10' => 'show_top10',
		'выводитьВТоп5' => 'show_top5',
		'выводитьВТоп3' => 'show_top3',
		'ХостДляПроекта' => 'host',
		'РасчетСтоимостиОтчетПозиций' => 'calculate_cost_positions',
		'КлючApi' => 'api_key',
		'НеПродвигается' => 'no_seo',
		'Представление' => 'represent',
	);
	try {
		$db = new PDO('mysql:host=localhost;dbname=fructus_saas;charset=utf8', 'root', '',array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	}
	catch(Exception $e)
	{
		print_r($e);
	}

	foreach ($xml->Сайт as $s)
	{
		$url = (array)$s->attributes()->Наименование;
		
		$q = "INSERT INTO `sites` SET ";
		$qP = array();
		$qP[] = "`url` = '{$url[0]}'";
		
		$attributes = array();
		foreach($s->Реквизит as $r)
		{
			foreach ($r->attributes() as $a => $b)
			{
				$b = (array)$b;
				$b = $b[0];
				
				if($a == 'ДатаСоздания')
				{
					list($d,$t) = explode(' ',$b);
					$d = DateTime::createFromFormat('d.m.Y', $d);
					$b = $d->format('Y-m-d H:i:s');
				}
				if($a == 'Тематика')
				{
					if($b != '')
					{
						try{
							$r = $db->query("SELECT `id` FROM `sites_themes` WHERE `name` = '{$b}'");
							$id = $r->fetch();
							if(isset($id['id'])) $b = $id['id'];
							else $b = '';
						}
						catch (Exception $e)
						{
							print_r($e);
						}
					}
				}
				
				if($a == 'ТипСайта')
				{
					if($b != '')
					{
						try{
							$r = $db->query("SELECT `id` FROM `sites_types` WHERE `name` = '{$b}'");
							$id = $r->fetch();
							if(isset($id['id'])) $b = $id['id'];
							else $b = '';
						}
						catch (Exception $e)
						{
							print_r($e);
						}
					}
				}
				
				if($a == 'liex_id' || $a == 'sape_id' && $b != '' && $b != '0')
				{
					$b = preg_replace('/[^\d]/', '', $b);
				}
				
				if(in_array($a, $stopAttr)) continue;
				if($b == 'Да') $b = 1;
				elseif($b == 'Нет') $b = 0;
				else $attributes[$a] = $b;
				
				
				$qP[] = "`{$attributesMap[$a]}` = '{$b}'";
			}
				
		}
		
		$q .= implode(',',$qP);
		echo $q.'<br>';
		$db->exec($q);
		$sites[] = array('url' => $url[0],'attributes' => $attributes);
	}
?>
