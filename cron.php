<?php

set_time_limit(0);
date_default_timezone_set('Europe/Moscow');

//config
$docpath = dirname(__FILE__);
$domain = 'http://127.0.0.1';

$db = new PDO("mysql:host=localhost;dbname=fructus_saas", 'root', 'Pa$$w0rd');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->query("SET NAMES 'utf8'");

$q = "SELECT * FROM `cron_tasks` WHERE `state` = 0 LIMIT 1";
$q = $db->query($q);
$task = $q->fetch(PDO::FETCH_ASSOC);

$q = "UPDATE `cron_tasks` SET `state` = 1 WHERE `id` = :id AND `state` = 0";
$q = $db->prepare($q);
$q->bindValue(':id',$task['id']);
$q->execute();

if($q->rowCount() <= 0) $task = null;

if(!is_null($task))
{
	require_once $docpath . '/cron/'.str_replace('/','_',$task['task']).'.php';
	$q = "DELETE FROM `cron_tasks` WHERE `id` = :id AND `state` = 1";
	$q = $db->prepare($q);
	$q->bindValue(':id',$task['id']);
	$q->execute();
}