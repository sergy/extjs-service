<?php defined('SYSPATH') OR die('No direct access allowed.');
	$dsn = '';
	$user = '';
	$password = '';
	
if (Kohana::$environment === Kohana::DEVELOPMENT) {
    $dsn = 'mysql:host=localhost;dbname=fructus_saas';
	$user = 'root';
	$password = '';
} elseif (Kohana::$environment === Kohana::TESTING) {
    $dsn = 'mysql:host=localhost;dbname=fructus_saas';
	$user = 'root';
	$password = 'Pa$$w0rd';
}
 elseif (Kohana::$environment === Kohana::PRODUCTION) {
    
}

return array
	(
		'default' => array
		(
		'type'       => 'PDO',
			'connection' => array(
				'dsn'        => $dsn,
				'username'   => $user,
				'password'   => $password,
				'persistent' => FALSE,
			),
			'table_prefix' => '',
			'charset'      => 'utf8',
			'caching'      => FALSE,
		)
	);
