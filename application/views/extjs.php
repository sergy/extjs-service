<html>
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" type="text/css" href="/assets/libs/extjs/resources/css/ext-all.css">
	<link rel="stylesheet" type="text/css" href="/assets/libs/extjs/plugins/clearButton/clearButton.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/application.css">
    <script type="text/javascript" src="/assets/libs/extjs/ext-all.js"></script>
	<script type="text/javascript" src="/assets/libs/extjs/plugins/clearButton/clearButton.js"></script>
</head>
<body><?= $content ?></body>
</html>