<?php

class Cron extends Controller{
	protected $result = array();
			
	function before() {
		parent::before();
		$this->result['success'] = false;
		$this->result['message'] = 'Запрос завершился неудачно';
		$this->response->headers('Content-Type','application/json');
	}
	
	function after() {
		$this->response->body(json_encode($this->result));
		parent::after();
	}
}

?>
