<?php

class Model_Sitemap extends Model
{
	const	DOCUMENT_TABLE = 'documents_sitemap',
			DOCUMENT_WORDS_TABLE = 'documents_sitemap_words',
			CRON_TABLE = 'cron_tasks';
	
	/*
	 * Return list of sitemap documents
	 * @params
	 *		$start - start limit
	 *		$limit - offset limit
	 * 
	 * @returns
	 *		$count - count of total rows
	 *		$res - query result
	 */
	public function read($start = 0,$limit = 25)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS d.*,s.url as `site` FROM `".self::DOCUMENT_TABLE."` d
			LEFT JOIN `sites` s ON s.id = d.site_id
			LIMIT $start, $limit";
		$res = DB::query(Database::SELECT ,$q)->execute()->as_array();
		$count = DB::query(Database::SELECT, 'SELECT FOUND_ROWS() AS `count`')->execute()->current();
		return array($count['count'],$res);
	}
	
	/*
	 * Return information of sitemap document
	 * @params
	 *		$id - document number
	 * 
	 * @returns
	 *		$res - query result
	 */
	public function readDocument($id)
	{
		$q = "SELECT * FROM `".self::DOCUMENT_TABLE."` WHERE `id` = :id";
		$res = DB::query(Database::SELECT ,$q)->bind(':id',$id)->execute()->current();
		if(empty($res)) return false;
		else return $res;
	}
	
	/*
	 * Create new sitemap document
	 * 
	 * @returns
	 *     $id - id of created row
	 *     $date - date of created row 
	 */
    public function create()
    {
		$date = date('Y-m-d H:i:s');
		$q = "INSERT INTO `".self::DOCUMENT_TABLE."` SET `date_created` = :date";
		list($id, $rows) = DB::query(Database::INSERT,$q)->param(':date', $date)->execute();
		
		return array($id,$date);
    }
	
	/*
	 * Return list of sitemap document's stop words
	 * @params
	 *		$id - document id
	 *		$start - start limit
	 *		$limit - offset limit
	 * 
	 * @returns
	 *		$count - count of total rows
	 *		$res - query result
	 */
	public function readWords($id, $start = 0,$limit = 0)
	{
		if($limit > 0) $l = 'LIMIT '.$start.','.$limit;
		else $l = '';
		
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM `".self::DOCUMENT_WORDS_TABLE."` WHERE `document_id` = :id $l";
		$res = DB::query(Database::SELECT ,$q)->bind(':id',$id)->execute()->as_array();
		$count = DB::query(Database::SELECT, 'SELECT FOUND_ROWS() AS `count`')->execute()->current();
		return array($count['count'],$res);
	}
	
	/*
	 * Delete stop words of sitemap document
	 * @params
	 *		$id - document id
	 * 
	 */
	public function deleteWords($id)
	{

		$q = "DELETE FROM `".self::DOCUMENT_WORDS_TABLE."` WHERE `document_id` = :id";
		$res = DB::query(Database::DELETE ,$q)->bind(':id',$id)->execute();
	}
	
	/*
	 * Add new stop words to document
	 * 
	 * @params
	 *     $id - id fo document
	 *     $words - array of words
	 */
    public function addWords($id,$words)
    {
		$date = date('Y-m-d H:i:s');
		$q = "INSERT INTO `".self::DOCUMENT_WORDS_TABLE."` SET `document_id` = :id, `word` = :word";
		foreach ($words as $word)
			if(!empty($word))
				DB::query(Database::INSERT,$q)->param(':id', $id)->param(':word', $word)->execute();
    }
	
	/*
	 * Delete document by id
	 * @params
	 *		$id - document id
	 * 
	 * TODO: remove all child data and trash
	 */
	public function delete($id)
	{
		$q = "DELETE FROM `".self::DOCUMENT_TABLE."` WHERE `id` = '{$id}'";
		DB::query(Database::DELETE ,$q)->execute();
	}
	
	/*
	 * Update document by id
	 * @params
	 *		$id - document id
	 *		$site_id - id of site
	 *		$noindex - noindex value
	 * 
	 */
	public function update($id,$site_id,$noindex)
	{
		$q = "UPDATE `".self::DOCUMENT_TABLE."` SET `site_id` = :site, `noindex` = :noindex WHERE `id` = '{$id}'";
		DB::query(Database::UPDATE ,$q)->bind(':site',$site_id)->bind(':noindex',$noindex)->execute();
	}
	
	/*
	 * Create cron task for sitemap build
	 * @params
	 *		$id - document id
	 *		$site_id - id of site
	 *		$noindex - noindex value
	 * 
	 */
	public function createSitemapTask($id,$site_id,$noindex)
	{
		$data = json_encode(array(
			'siteId' => $site_id,
			'noindex' => $noindex
		));
		
		$task = 'sitemap/create';
		$state = '1';
		
		$q = "SELECT COUNT(*) as `count` FROM `".self::CRON_TABLE."` WHERE `task` = :task AND `document_id` = :documentId";
		$res = DB::query(Database::SELECT ,$q)->bind(':task',$task)->bind(':documentId',$id)->execute()->current();
		if(empty($res) || $res['count'] <= 0)
		{
			$q = "INSERT INTO `".self::CRON_TABLE."` SET `task` = :task, `data` = :data, `document_id` = :documentId";
			DB::query(Database::INSERT ,$q)->bind(':task',$task)->bind(':data',$data)->bind(':documentId',$id)->execute();
			
			$q = "UPDATE `".self::DOCUMENT_TABLE."` SET `state` = :state, `date_started` = NOW() WHERE `id` = :documentId";
			DB::query(Database::UPDATE ,$q)->bind(':state',$state)->bind(':documentId',$id)->execute();
		}
		else return false;
		
		return true;
	}
	
	/*
	 * Finish sitemap's data from cron task
	 * @params
	 *		$id - document id
	 *		$state - state number
	 * 
	 */
	public function setSitemapStatus($id,$state,$dbpath)
	{
		$q = "UPDATE `".self::DOCUMENT_TABLE."` SET `state` = :state, `date_finished` = NOW(), `dbpath` = :dbpath WHERE `id` = :documentId";
		DB::query(Database::UPDATE ,$q)->bind(':state',$state)->bind(':documentId',$id)->bind(':dbpath',$dbpath)->execute();
	}
	
	/*
	 * Remove cron task
	 * @params
	 *		$id - task id
	 * 
	 */
	public function removeCronTask($id)
	{
		$q = "DELETE FROM `".self::CRON_TABLE."` WHERE `id` = :id";
		DB::query(Database::DELETE ,$q)->bind(':id',$id)->execute();
	}
	
	/*
	 * Read sqllite db path for document by its id
	 * @params
	 *		$id - docuent id
	 * 
	 */
	public function readDBPath($id)
	{
		$q = "SELECT `dbpath` FROM `".self::DOCUMENT_TABLE."` WHERE `id` = :id";
		$res = DB::query(Database::SELECT ,$q)->bind(':id',$id)->execute()->current();
		
		return (isset($res['dbpath'])) ? $res['dbpath'] : false;
	}
}

?>
