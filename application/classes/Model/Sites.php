<?php

class Model_Sites extends Model
{
	const	SITES_TABLE = 'sites';
	
	/*
	 * Return list of sits
	 * @params
	 *		$start - start limit
	 *		$limit - offset limit
	 * 
	 * @returns
	 *		$count - count of total rows
	 *		$res - query result
	 */
	public function read($start = 0,$limit = 25,$query = '')
	{
		if(!empty($query))
		{
			$w = "WHERE `url` LIKE :q";
			$query = "%{$query}%";
		}
		else $w = '';
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM `".self::SITES_TABLE."` {$w} ORDER BY `url` LIMIT $start, $limit";
		$r = DB::query(Database::SELECT ,$q);
		if(!empty($w))
			$r->bind(':q',$query);
			
		$res = $r->execute()->as_array();
		$count = DB::query(Database::SELECT, 'SELECT FOUND_ROWS() AS `count`')->execute()->current();
		return array($count['count'],$res);
	}
	
	/*
	 * Return row of site by its id
	 * @params
	 *		$id - site's id
	 * 
	 * @returns
	 *		$res - query result or false
	 */
	public function getSiteById($id)
	{
		$q = "SELECT * FROM `".self::SITES_TABLE."` WHERE `id` = :id";
		$res = DB::query(Database::SELECT ,$q)->bind(':id',$id)->execute()->current();
		if(empty($res)) return false;
		else return $res;
	}
}

?>
