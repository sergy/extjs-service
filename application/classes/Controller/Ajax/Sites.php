<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Sites extends Ajax {
	protected $model = null;


	public function before() {
		parent::before();
		$this->model = new Model_Sites();
	}

	public function action_read()
	{
		try {
			$start = intval(Arr::get($_REQUEST,'start'));
			$limit = (Arr::get($_REQUEST,'limit')) ? intval(Arr::get($_REQUEST,'limit')) : 25;
			$query = (Arr::get($_REQUEST,'query')) ? Arr::get($_REQUEST,'query') : '';
			
			list($count,$res) = $this->model->read($start,$limit,$query);
			
			$this->result['success'] = true;
			$this->result['data'] = $res;
			$this->result['total'] = intval($count);
			$this->result['message'] = '';
			
		} catch (Exception $e)
		{
			$this->result['message'] = 'Не удалось получить список сайтов. Ошибка базы данных.'.$e;
		}
	}
}