<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Sitemap extends Ajax {

	protected $model = null;

	public function before() {
		parent::before();
		$this->model = new Model_Sitemap();
	}

	/*
	 *  Create task for sitemap generation by cron
	 */
	
	public function action_createSitemap()
	{
		$this->response->headers('Content-Type', 'text/html');
		$request_body = file_get_contents('php://input');
		$json = json_decode($request_body);
		if (isset($json->documentId)) {
			$id = intval($json->documentId);
			if (!empty($id)) {

				$site_id = intval($json->site_id);
				$noindex = intval($json->noindex);

				$sitesModel = new Model_Sites();
				if (!$sitesModel->getSiteById($site_id)) {
					$this->result['message'] = 'Неверный идентификатор сайта';
				} elseif ($noindex < 0) {
					$this->result['message'] = 'Неверное значение глубины noindex';
				} else {
					$res =$this->model->createSitemapTask($id, $site_id, $noindex);
					if($res === false)
					{
						$this->result['message'] = 'Дождитесь завершения предыдущей задачи';
					}
					else
					{
						$this->result['success'] = true;
						$this->result['message'] = 'Задача для формирования карты сайта установлена';
					}
				}
			}
			else
				$this->result['message'] = 'Неверный номер документа';
		}
		else
			$this->result['message'] = 'Неверный номер документа';
	}
	
	/*
	 * Return list of sitemap documents
	 * 
	 */

	public function action_read() {
		try {
			$start = intval(Arr::get($_REQUEST, 'start'));
			$limit = (Arr::get($_REQUEST, 'limit')) ? intval(Arr::get($_REQUEST, 'limit')) : 25;

			list($count, $res) = $this->model->read($start, $limit);

			$this->result['success'] = true;
			$this->result['data'] = $res;
			$this->result['total'] = intval($count);
			$this->result['message'] = '';
		} catch (Exception $e) {
			$this->result['success'] = false;
			$this->result['message'] = 'Не удалось получить список документов. Ошибка базы данных.' . $e;
		}
	}
	
	/*
	 * Return list of  document's pages
	 * 
	 */

	public function action_readPages() {
		try {
			$documentId = intval(Arr::get($_REQUEST, 'documentId'));
			$site = Arr::get($_REQUEST, 'site');
			
			$start = (Arr::get($_REQUEST, 'start')) ? intval(Arr::get($_REQUEST, 'start')) : 0;
			$limit = (Arr::get($_REQUEST, 'limit')) ? intval(Arr::get($_REQUEST, 'limit')) : 25;
			
			if($limit > 0) $l = 'LIMIT '.$start.','.$limit;
			else $l = '';
			
			$dbpath = $this->model->readDBPath($documentId);
			$db_conn = 'sqlite:' . DOCROOT . $dbpath;
			try {
				$dbh = new PDO($db_conn);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (Exception $e) {
				var_dump($e);
				exit;
			}
			
			$this->result['data'] = array();
			
			$q = $dbh->query('SELECT COUNT(*) as `c` FROM `url`');
			$res = $q->fetch(PDO::FETCH_ASSOC);
			$count = $res['c'];
			
			$q = $dbh->query('SELECT `rowid` as `id`, `url`, `status`, `deep`, `title`, `description`, `keywords`, `redirect`, `weight` FROM `url` '.$l);
			while($res = $q->fetch(PDO::FETCH_ASSOC))
			{
				$this->result['data'][] = $res;
			}
			
			//list($count, $res) = $this->model->readPages($start, $limit);

			$this->result['success'] = true;
			$this->result['total'] = intval($count);
			$this->result['message'] = '';
		} catch (Exception $e) {
			print_r($e);
			$this->result['success'] = false;
			$this->result['message'] = 'Не удалось получить список страниц карты сайта. Ошибка базы данных.' . $e;
		}
	}
	
	/*
	 * Return list of  document's pages
	 * 
	 */

	public function action_readLinks() {
		try {
			$documentId = intval(Arr::get($_REQUEST, 'documentId'));
			$site = Arr::get($_REQUEST, 'site');
			
			$start = (Arr::get($_REQUEST, 'start')) ? intval(Arr::get($_REQUEST, 'start')) : 0;
			$limit = (Arr::get($_REQUEST, 'limit')) ? intval(Arr::get($_REQUEST, 'limit')) : 25;
			
			$direction = (Arr::get($_REQUEST, 'direction')) ? Arr::get($_REQUEST, 'direction') : 'from';
			$pageId = (Arr::get($_REQUEST, 'pageId')) ? intval(Arr::get($_REQUEST, 'pageId')) : 0;
			
			if($limit > 0) $l = 'LIMIT '.$start.','.$limit;
			else $l = '';
			
			$dbpath = $this->model->readDBPath($documentId);
			$db_conn = 'sqlite:' . DOCROOT . $dbpath;
			try {
				$dbh = new PDO($db_conn);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (Exception $e) {
				var_dump($e);
				exit;
			}
			
			$this->result['data'] = array();
			
			$q = $dbh->query("SELECT COUNT(*) as `c` FROM `url_links` WHERE `{$direction}` = {$pageId}");
			$res = $q->fetch(PDO::FETCH_ASSOC);
			$count = $res['c'];
			
			$q = null;
			if($direction == 'from')
			{
				$query = "SELECT p.`url` as `url`, l.`title`, l.`text` FROM `url_links` l JOIN `url` p ON l.`to` = p.rowid AND l.external = 0 WHERE `from` = {$pageId} {$l}";
				$q = $dbh->query($query);
			}
			elseif($direction == 'to')
			{
				$query = "SELECT p.`url` as `url`, l.`title`, l.`text` FROM `url_links` l JOIN `url` p ON l.`from` = p.rowid AND l.external = 0 WHERE `to` = {$pageId} {$l}";
				$q = $dbh->query($query);
			}
			else
			{
				$this->result['data'][] = array();
			}
			
			if(!is_null($q))
			{
				while($res = $q->fetch(PDO::FETCH_ASSOC))
				{
					$this->result['data'][] = $res;
				}
			}
			
			//list($count, $res) = $this->model->readPages($start, $limit);

			$this->result['success'] = true;
			$this->result['total'] = intval($count);
			$this->result['message'] = '';
		} catch (Exception $e) {
			print_r($e);
			$this->result['success'] = false;
			$this->result['message'] = 'Не удалось получить список страниц карты сайта. Ошибка базы данных.' . $e;
		}
	}

	/*
	 * Create sitemap documents
	 * 
	 */

	public function action_new() {
		try {
			list($id, $date) = $this->model->create();
			if (!empty($id)) {
				$data = $this->model->readDocument($id);
				$this->result['success'] = true;
				$this->result['data'] = $data;
				$this->result['message'] = 'Документ успешно создан';
			} else {
				$this->result['message'] = 'Не удалось создать документ. Ошибка базы данных.';
			}
		} catch (Exception $e) {
			$this->result['success'] = false;
			$this->result['message'] = 'Не удалось создать документ. Ошибка базы данных.';
		}
	}

	/*
	 * Get sitemap's documents information
	 * 
	 */
	public function action_document() {

		$id = $this->request->param('id');
		$id = intval($id);
		if (!empty($id)) {
			try {
				$data = $this->model->readDocument($id);

				$this->result['success'] = true;
				$this->result['data'] = $data;
				$this->result['message'] = 'Информация успешно получена';
			} catch (Exception $e) {
				$this->result['success'] = false;
				$this->result['message'] = 'Не удалось получить документ. Ошибка базы данных.' . $e;
			}
		}
		else
			$this->result['message'] = 'Неверный номер документа';
	}

	/*
	 * Return list of sitemap document's stop words
	 * 
	 */

	public function action_readWords() {
		try {
			$id = intval(Arr::get($_REQUEST, 'documentId'));
			$start = intval(Arr::get($_REQUEST, 'start'));
			$limit = (Arr::get($_REQUEST, 'limit')) ? intval(Arr::get($_REQUEST, 'limit')) : 25;

			list($count, $res) = $this->model->readWords($id, $start, $limit);

			$this->result['success'] = true;
			$this->result['data'] = $res;
			$this->result['total'] = intval($count);
			$this->result['message'] = '';
		} catch (Exception $e) {
			$this->result['success'] = false;
			$this->result['message'] = 'Не удалось получить список стоп слов. Ошибка базы данных.' . $e;
		}
	}

	/*
	 * Delete sitemap document
	 * 
	 */

	public function action_destroy() {
		try {
			$request_body = file_get_contents('php://input');
			$item = json_decode($request_body);
			$this->model->delete($item->id);

			$this->result['success'] = true;
			$this->result['message'] = 'Документ успешно удалён';
		} catch (Exception $e) {
			$this->result['success'] = false;
			$this->result['message'] = 'Не удалось удалить документ. Ошибка базы данных.' . $e;
		}
	}

	/*
	 * Import sitemap document's stop words
	 * 
	 */

	public function action_importWords() {
		$this->response->headers('Content-Type', 'text/html');
		if ($_FILES) {
			$ext = explode('.', $_FILES['file']['name']);
			$ext = end($ext);
			if ($ext == 'txt') {
				$data = file_get_contents($_FILES['file']['tmp_name']);
				$words = explode(',', $data);
				$id = intval($_REQUEST['documentId']);
				if (!empty($id)) {
					foreach ($words as $k => $v)
						$words[$k] = trim($v);
					$this->model->addWords($id, $words);

					$this->result['success'] = true;
					$this->result['message'] = 'Слова загружены';
				}
				else
					$this->result['message'] = 'Неверный номер документа';
			}
			else
				$this->result['message'] = 'Неверный тип файла';
		}
		else
			$this->result['message'] = 'Не выбран файл';
	}

	/*
	 * Export sitemap document's stop words
	 * 
	 */

	public function action_exportWords() {
		$this->response->headers('Content-Type', 'text/html');
		$request_body = file_get_contents('php://input');
		$json = json_decode($request_body);
		if (isset($json->documentId)) {
			$id = intval($json->documentId);
			if (!empty($id)) {
				list($count, $res) = $this->model->readWords($id);
				$words = array();
				foreach ($res as $w)
					$words[] = $w['word'];

				$filename = time() . '_sw_' . $id . '.txt';
				$path = 'files/tmp/' . $filename;
				file_put_contents($path, implode(',', $words));

				$this->result['success'] = true;
				$this->result['path'] = $path;
				$this->result['message'] = 'Файл готов';
			}
			else
				$this->result['message'] = 'Неверный номер документа';
		}
		else
			$this->result['message'] = 'Неверный номер документа';
	}

	/*
	 * Delete stop words from sitemap document
	 * 
	 */

	public function action_deleteWords() {
		$this->response->headers('Content-Type', 'text/html');
		$id = Arr::get($_REQUEST, 'documentId');
		$id = intval($id);
		if (!empty($id)) {
			$this->model->deleteWords($id);

			$this->result['success'] = true;
			$this->result['message'] = 'Стоп-слова удалены';
		}
		else
			$this->result['message'] = 'Неверный номер документа';
	}

	/*
	 * Save sitemap document
	 * 
	 */

	public function action_save() {
		$this->response->headers('Content-Type', 'text/html');
		$request_body = file_get_contents('php://input');
		$json = json_decode($request_body);
		if (isset($json->documentId)) {
			$id = intval($json->documentId);
			if (!empty($id)) {

				$site_id = intval($json->site_id);
				$noindex = intval($json->noindex);

				$sitesModel = new Model_Sites();
				if (!$sitesModel->getSiteById($site_id)) {
					$this->result['message'] = 'Неверный идентификатор сайта';
				} elseif ($noindex < 0) {
					$this->result['message'] = 'Неверное значение глубины noindex';
				} else {
					$this->model->update($id, $site_id, $noindex);
					$this->result['success'] = true;
					$this->result['message'] = 'Изменения сохранены';
				}
			}
			else
				$this->result['message'] = 'Неверный номер документа';
		}
		else
			$this->result['message'] = 'Неверный номер документа';
	}

}
