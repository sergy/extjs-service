<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Common {

	public $template = 'extjs';
	
	public function before() {
		parent::before();
		$this->template->template = 'extjs/';
	}
	
	public function action_index()
	{
		$this->template->content = new View($this->template->template.'index');
		$this->template->title = 'Fructus SAAS';
	}

} // End Welcome
