<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Cron_Sitemap extends Cron {

	protected $model = null;

	public function before() {
		parent::before();
		$this->model['sitemap'] = new Model_Sitemap();
		$this->model['sites'] = new Model_Sites();
	}
	
	public function action_create()
	{
		$siteId = (int)Arr::get($_GET,'siteId');
		$noindex = (int)Arr::get($_GET,'noindex');

		if(!empty($siteId))
		{
			$site = $this->model['sites']->getSiteById($siteId);
			$this->result['success'] = true;
			$this->result['message'] = 'Запрос завершился успешно';
			$this->result['data'] = array(
				'noindex' => $noindex,
				'site' => $site
			);
		}
	}
	
	public function action_finish()
	{
		$documentId = (int)Arr::get($_GET,'documentId');
		$taskId = (int)Arr::get($_GET,'taskId');
		$dbpath = Arr::get($_GET,'dbpath');
		
		if(!empty($documentId) && !empty($taskId))
		{
			$site = $this->model['sitemap']->setSitemapStatus($documentId,'2',$dbpath);
			$site = $this->model['sitemap']->removeCronTask($taskId);
			$this->result['success'] = true;
			$this->result['message'] = 'Запрос завершился успешно';
		}
	}
}
