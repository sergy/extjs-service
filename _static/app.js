var words_store = Ext.create('Ext.data.ArrayStore', {
	fields: ['id', 'word'],
	data: [
		[1, 'forum'],
		[2, 'форум'],
		[3, 'чат'],
		[4, 'chat'],
		[5, 'lorem'],
		[6, 'ipsum'],
		[7, 'слово'],
		[8, 'стоп'],
		[9, 'пример'],
		[10, 'фраза']
	]
});

var pages_store = Ext.create('Ext.data.ArrayStore', {
	fields: ['id', 'url', 'status', 'deep', 'title', 'description', 'keywords', 'redirect', 'weight'],
	data: [
		[1, 'http://fructus.ru', 'OK', '', 'Раскрутка и поисковое продвижение сайтов. Оптимизация, создание и разработка сайтов в веб-студии Фруктус | Fructus Ltd.', 'Продвижение сайтов, поисковая раскрутка и оптимизация, разработка и изготовление веб сайтов - Фруктус | Fructus Ltd.', '', '', ''],
		[2, 'http://www.fructus.ru/templates/fructus-ru/css/global.css', 'файл (OK)', '', '', '', '', '', ''],
		[3, 'http://www.fructus.ru/templates/fructus-ru/css/additional.css?v=10', 'файл (OK)', '', '', '', '', '', ''],
		[4, 'http://www.fructus.ru/templates/fructus-ru/js/fancybox/jquery.fancybox.css', 'файл (OK)', '', '', '', '', '', ''],
		[5, 'http://www.fructus.ru/templates/fructus-ru/css/jquery-ui-1.10.2.custom.min.css', 'файл (OK)', '', '', '', '', '', ''],
		[6, 'http://fructus.ru', 'OK', '', 'Раскрутка и поисковое продвижение сайтов. Оптимизация, создание и разработка сайтов в веб-студии Фруктус | Fructus Ltd.', 'Продвижение сайтов, поисковая раскрутка и оптимизация, разработка и изготовление веб сайтов - Фруктус | Fructus Ltd.', '', '', ''],
		[7, 'http://www.fructus.ru/templates/fructus-ru/css/global.css', 'файл (OK)', '', '', '', '', '', ''],
		[8, 'http://www.fructus.ru/templates/fructus-ru/css/additional.css?v=10', 'файл (OK)', '', '', '', '', '', ''],
		[9, 'http://www.fructus.ru/templates/fructus-ru/js/fancybox/jquery.fancybox.css', 'файл (OK)', '', '', '', '', '', ''],
		[10, 'http://www.fructus.ru/templates/fructus-ru/css/jquery-ui-1.10.2.custom.min.css', 'файл (OK)', '', '', '', '', '', ''],
		[11, 'http://fructus.ru', 'OK', '', 'Раскрутка и поисковое продвижение сайтов. Оптимизация, создание и разработка сайтов в веб-студии Фруктус | Fructus Ltd.', 'Продвижение сайтов, поисковая раскрутка и оптимизация, разработка и изготовление веб сайтов - Фруктус | Fructus Ltd.', '', '', ''],
		[12, 'http://www.fructus.ru/templates/fructus-ru/css/global.css', 'файл (OK)', '', '', '', '', '', ''],
		[13, 'http://www.fructus.ru/templates/fructus-ru/css/additional.css?v=10', 'файл (OK)', '', '', '', '', '', ''],
		[14, 'http://www.fructus.ru/templates/fructus-ru/js/fancybox/jquery.fancybox.css', 'файл (OK)', '', '', '', '', '', ''],
		[15, 'http://www.fructus.ru/templates/fructus-ru/css/jquery-ui-1.10.2.custom.min.css', 'файл (OK)', '', '', '', '', '', '']
	]
});

var links_store = Ext.create('Ext.data.ArrayStore', {
	fields: ['url', 'text', 'title'],
	data: [
		['http://pcquality.ru/uslugi/', '_Alt: pcQuality'],
		['http://pcquality.ru/uslugi/', 'Обслуживание персональных компьютеров'],
		['http://pcquality.ru/ceny/', '_Alt: pcQuality'],
		['http://pcquality.ru/ceny/', 'Обслуживание персональных компьютеров'],
		['http://pcquality.ru/uslugi/', '_Alt: pcQuality'],
		['http://pcquality.ru/uslugi/', 'Обслуживание персональных компьютеров'],
		['http://pcquality.ru/ceny/', '_Alt: pcQuality'],
		['http://pcquality.ru/ceny/', 'Обслуживание персональных компьютеров'],
		['http://pcquality.ru/uslugi/', '_Alt: pcQuality'],
		['http://pcquality.ru/uslugi/', 'Обслуживание персональных компьютеров'],
		['http://pcquality.ru/ceny/', '_Alt: pcQuality'],
		['http://pcquality.ru/ceny/', 'Обслуживание персональных компьютеров']
	]
});

Ext.application({
	name: 'Fructus',
	launch: function() {
		Ext.create('Ext.container.Viewport', {
			layout: 'border',
			items: [
				{
					region: 'north',
					xtype: 'container',
					style: 'font-family: arial; font-size: 14pt; color: #15428B;',
					height: 40,
					html: '<div class="b-header"><div class="b-header-right">Вы вошли как <b>guest</b> (<a class="b-header-right-link" href="/logout">Выйти</a>)</div>Fructus SAAS :: Карта сайта</div>'
				}, {
					region: 'west',
					collapsible: true,
					title: 'Навигация',
					width: 255,
					margins: '0 0 5 5',
					split: true,
					layout: 'fit',
					stateful: true,
					stateId: 'menuPanelState',
					stateEvents: ['collapse', 'expand'],
					items: [{
							xtype: 'treepanel',
							border: false,
							id: 'id_treepanel_menu',
							autoScroll: true,
							rootVisible: false,
							root: '',
							listeners: {
								select: function(treepanel, record, index) {

								}
							}
						}]
				}, {
					region: 'center',
					layout: 'fit',
					margins: '0 5 5 0',
					frame: true,
					layout: 'fit',
							items: [{
							id: 'id_tabpanel_main',
							xtype: 'tabpanel',
							activeTab: 0,
							border: 0,
							items: [
								{
									title: 'Таблицы',
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									padding: '10 10',
									border: 0,
									items: [
										{
											xtype: 'container',
											flex: 3,
											height: 150,
											maxHeight: 150,
											layout: {
												type: 'hbox',
												align: 'stretch'
											},
											items: [
												{
													xtype: 'container',
													border: 0,
													flex: 1,
													items: [{
															xtype: 'textfield',
															fieldLabel: 'Номер'
														}, {
															xtype: 'combobox',
															fieldLabel: 'Сайт',
															plugins: ['clearbutton']
														}]
												}, {
													xtype: 'container',
													border: 0,
													flex: 1,
													items: [{
															xtype: 'datefield',
															fieldLabel: 'Дата'
														}, {
															xtype: 'numberfield',
															fieldLabel: 'Глубина noindex',
															minValue: 0
														}]
												}, {
													xtype: 'container',
													border: 0,
													flex: 2,
													layout: {
														type: 'hbox',
														align: 'stretch'
													},
													items: [
														{
															xtype: 'container',
															border: 0,
															flex: 2,
															layout: 'vbox',
															items: [{
																	xtype: 'text',
																	text: 'Стоп-слова:'
																}, {
																	xtype: 'button',
																	text: 'Очистить',
																	margins: '5 0',
																}, {
																	xtype: 'panel',
																	border: 0,
																	cls: 'left-right-buttons',
																	layout: 'hbox',
																	items: [{
																			xtype: 'button',
																			icon: '/app/assets/images/icons/magnifier.png'
																		}, {
																			xtype: 'splitter',
																			width: '25'
																		}, {
																			xtype: 'button',
																			icon: '/app/assets/images/icons/cross.png'
																		}]
																}]
														}, {
															xtype: 'grid',
															flex: 15,
															store: words_store,
															collapsible: false,
															height: 150,
															maxHeight: 150,
															margins: '0 5',
															columns: [
																{
																	text: '№',
																	dataIndex: 'id',
																	flex: 1,
																	sortable: false
																},
																{
																	text: 'Слово',
																	dataIndex: 'word',
																	flex: 20,
																	sortable: false
																}
															],
															viewConfig: {
																stripeRows: true
															}
														}, {
															xtype: 'container',
															border: 0,
															flex: 1,
															items: [
																{
																	xtype: 'button',
																	text: '- >'
																}, {
																	xtype: 'splitter',
																	width: '25'
																}, {
																	xtype: 'button',
																	text: '< -'
																}
															]
														}
													]
												}
											]
										}, {
											xtype: 'container',
											margins: '10 0',
											layout: {
												type: 'hbox',
												align: 'stretch'
											},
											items: [
												{
													xtype: 'grid',
													flex: 2,
													store: pages_store,
													collapsible: false,
													margins: '0 5',
													columns: [
														{
															text: '№',
															dataIndex: 'id',
															flex: 1,
															sortable: false
														},
														{
															text: 'Url',
															dataIndex: 'url',
															flex: 20,
															sortable: false
														},
														{
															text: 'Статус',
															dataIndex: 'status',
															flex: 5,
															sortable: false
														},
														{
															text: 'Глубина',
															dataIndex: 'deep',
															flex: 5,
															sortable: false
														},
														{
															text: 'Title',
															dataIndex: 'title',
															flex: 5,
															sortable: false
														},
														{
															text: 'Description',
															dataIndex: 'description',
															flex: 5,
															sortable: false
														},
														{
															text: 'Keywords',
															dataIndex: 'keywords',
															flex: 5,
															sortable: false
														},
														{
															text: 'Редирект',
															dataIndex: 'redirect',
															flex: 5,
															sortable: false
														},
														{
															text: 'Вес',
															dataIndex: 'weight',
															flex: 5,
															sortable: false
														}
													],
													viewConfig: {
														stripeRows: true
													}
												}, {
													xtype: 'grid',
													flex: 1,
													store: links_store,
													collapsible: false,
													margins: '0 5',
													columns: [
														{
															text: 'Url',
															dataIndex: 'url',
															flex: 20,
															sortable: false
														},
														{
															text: 'Текст ссылки',
															dataIndex: 'text',
															flex: 5,
															sortable: false
														},
														{
															text: 'Title ссылки',
															dataIndex: 'title',
															flex: 5,
															sortable: false
														}
													],
													viewConfig: {
														stripeRows: true
													}
												}
											]
										},
										, {
											xtype: 'container',
											layout: {
												type: 'hbox',
												align: 'stretch'
											},
											items: [
												{
													xtype: 'container',
													layout: {
														type: 'hbox',
														align: 'stretch'
													},
													flex: 2,
													items: [
														{
															xtype: 'button',
															margins: '0 5 0 0',
															text: 'Найти (Alt + E)'
														},{
															xtype: 'button',
															margins: '0 5 0 0',
															text: 'Очистить failied'
														},{
															xtype: 'button',
															margins: '0 5 0 0',
															text: 'К следующему из плана (Alt + 1)'
														},{
															xtype: 'button',
															margins: '0 150 0 0',
															text: 'Записать (Alt + s)'
														},{
															xtype: 'datefield',
															labelWidth: 150,
															fieldLabel: 'Дата построения карты'
														}
													]
												},{
													xtype: 'container',
													flex: 1,
													html: '171 страница / 342 ссылки, всего 173'
												}
												
											]
										}
									]
								},
								{
									title: 'Пересечение',
									html: "My content was added during construction.",
									disabled: true
								},
								{
									title: 'Рекомендации',
									html: "My content was added during construction.",
									disabled: true
								},
								{
									title: 'Контекст',
									html: "My content was added during construction.",
									disabled: true
								}
							]
						}
					]
				}
			]
		});
	}
});